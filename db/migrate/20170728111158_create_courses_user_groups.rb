class CreateCoursesUserGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :courses_user_groups do |t|
      t.integer :course_id
      t.integer :user_group_id
    end
  end
end
