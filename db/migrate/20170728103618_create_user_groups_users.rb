class CreateUserGroupsUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :user_groups_users do |t|
      t.integer :user_id
      t.integer :user_group_id
    end
  end
end
