class Admin::CoursesController < ApplicationController
  load_and_authorize_resource

  def index
    @courses = @courses.includes(:users, :user_groups)
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
    if @course.save
      associate_user_groups

      redirect_to admin_courses_path
    else
      render 'new'
    end
  end

  def update
    if @course.update(course_params)
      associate_user_groups
      associate_users

      redirect_to admin_courses_path
    else
      render 'edit'
    end
  end

  def destroy
    @course.destroy

    redirect_to admin_courses_path
  end

  private

  def course_params
    params.require(:course)
      .permit(:title, :description, user_groups_attributes: [:id, :_destroy])
  end

  def associate_user_groups
    @course.user_groups = []
    params[:course][:user_groups].delete_if(&:blank?).each do |user_group_id|
      @course.user_groups << UserGroup.find(user_group_id)
    end
  end

  def associate_users
    @course.users = []
    params[:course][:users].delete_if(&:blank?).each do |user_id|
      @course.users << User.find(user_id)
    end
  end
end
