class ApplicationController < ActionController::Base
  include ApplicationHelper

  protect_from_forgery with: :exception

  rescue_from CanCan::AccessDenied do |_exception|
    redirect_to new_user_session_path, alert: I18n.t('cancan.access_denied')
  end

  def current_ability
    @current_ability ||= Ability.new(current_user, current_namespace(need_application: true))
  end
end
