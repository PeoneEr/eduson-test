class Ability
  include CanCan::Ability

  def initialize(user, namespace)
    return unless user

    case namespace
    when 'application'
      can :read, Course, users: { id: user.id }
      can :read, Course, user_groups: { user_groups_users: { user_id: user.id } }
    when 'admin'
      can :manage, Course
      can :manage, UserGroup
    end
  end
end
