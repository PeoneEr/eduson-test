class CoursesUserGroup < ApplicationRecord
  belongs_to :course
  belongs_to :user_group
end
