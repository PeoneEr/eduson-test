class UserGroup < ApplicationRecord
  validates :title, presence: true, uniqueness: true

  has_many :user_groups_users
  has_many :users, through: :user_groups_users
end
