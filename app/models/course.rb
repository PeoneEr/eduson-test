class Course < ApplicationRecord
  validates :title, presence: true, uniqueness: true

  has_many :courses_users
  has_many :users, through: :courses_users

  has_many :courses_user_groups
  has_many :user_groups, through: :courses_user_groups
end
