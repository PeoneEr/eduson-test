module ApplicationHelper
  def current_namespace(need_application: true)
    namespace = controller_path.split('/').first

    namespace = %w(application admin).include?(namespace) ? namespace : 'application'

    return nil if !need_application && namespace == 'application'

    namespace
  end

  def can_manage_courses?
    current_user.admin? && current_namespace(need_application: true) == 'admin'
  end
end
