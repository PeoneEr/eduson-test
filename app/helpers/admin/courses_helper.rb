module Admin::CoursesHelper
  def user_groups_collection
    UserGroup.order('title')
  end

  def users_collection
    User.order('id')
  end
end
